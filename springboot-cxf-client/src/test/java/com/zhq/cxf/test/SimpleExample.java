package com.zhq.cxf.test;

import com.zhq.cxf.generate.Results;
import com.zhq.cxf.generate.UserInfo;
import com.zhq.cxf.generate.UserPairingServiceImpl;
import com.zhq.cxf.generate.UserPairingServiceImplService;
import com.zhq.cxf.intercept.AddHeaderIntercept;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * <pre>
 * @class com.zhq.cxf.test.SimpleExample
 * @description 使用wsimport命令生成的代码调用服务接口示例
 * @author zhq
 * @date 2022/6/2 17:21
 * </pre>
 */
@SpringBootTest
public class SimpleExample {

    @Test
    public void invoke() {

        UserInfo userInfo = new UserInfo();
        userInfo.setName("张三");
        userInfo.setAge(25);
        userInfo.setSex("男");
        userInfo.setHeight(180);
        userInfo.setWeight(140);
        userInfo.setAddr("上海市翻斗大街翻斗花园二号楼1001室");
        userInfo.setNational("维吾尔族");

        int minAge = 18;
        int maxAge = 32;


        UserPairingServiceImplService service = new UserPairingServiceImplService();
        UserPairingServiceImpl port = service.getUserPairingServiceImplPort();

        Client client = ClientProxy.getClient(port);
        // 添加拦截器
        client.getOutInterceptors().add(new AddHeaderIntercept());

        Results appropriateUser = port.getAppropriateUser(userInfo, minAge, maxAge);

        System.out.println("count: " + appropriateUser.getCount());
        List<UserInfo> list = appropriateUser.getList();
        for (UserInfo info : list) {
            System.out.println(info);
        }

    }

}

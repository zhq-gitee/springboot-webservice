package com.zhq.cxf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <pre>
 * @class com.zhq.cxf.CxfClientApplication
 * @description
 * @author zhq
 * @date 2022/6/2 16:39
 * </pre>
 */
@SpringBootApplication
public class CxfClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(CxfClientApplication.class, args);
    }
}

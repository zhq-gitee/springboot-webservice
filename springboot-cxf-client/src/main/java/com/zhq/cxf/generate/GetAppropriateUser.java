
package com.zhq.cxf.generate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>getAppropriateUser complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="getAppropriateUser">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="userInfo" type="{http://cxf.zhq.com/ws/userPairing}userInfo" minOccurs="0"/>
 *         &lt;element name="minAge" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="maxAge" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getAppropriateUser", propOrder = {
    "userInfo",
    "minAge",
    "maxAge"
})
public class GetAppropriateUser {

    protected UserInfo userInfo;
    protected int minAge;
    protected int maxAge;

    /**
     * 获取userInfo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link UserInfo }
     *     
     */
    public UserInfo getUserInfo() {
        return userInfo;
    }

    /**
     * 设置userInfo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link UserInfo }
     *     
     */
    public void setUserInfo(UserInfo value) {
        this.userInfo = value;
    }

    /**
     * 获取minAge属性的值。
     * 
     */
    public int getMinAge() {
        return minAge;
    }

    /**
     * 设置minAge属性的值。
     * 
     */
    public void setMinAge(int value) {
        this.minAge = value;
    }

    /**
     * 获取maxAge属性的值。
     * 
     */
    public int getMaxAge() {
        return maxAge;
    }

    /**
     * 设置maxAge属性的值。
     * 
     */
    public void setMaxAge(int value) {
        this.maxAge = value;
    }

}


package com.zhq.cxf.generate;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.zhq.cxf.generate package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetAppropriateUser_QNAME = new QName("http://cxf.zhq.com/ws/userPairing", "getAppropriateUser");
    private final static QName _GetAppropriateUserResponse_QNAME = new QName("http://cxf.zhq.com/ws/userPairing", "getAppropriateUserResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.zhq.cxf.generate
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetAppropriateUserResponse }
     * 
     */
    public GetAppropriateUserResponse createGetAppropriateUserResponse() {
        return new GetAppropriateUserResponse();
    }

    /**
     * Create an instance of {@link GetAppropriateUser }
     * 
     */
    public GetAppropriateUser createGetAppropriateUser() {
        return new GetAppropriateUser();
    }

    /**
     * Create an instance of {@link UserInfo }
     * 
     */
    public UserInfo createUserInfo() {
        return new UserInfo();
    }

    /**
     * Create an instance of {@link Results }
     * 
     */
    public Results createResults() {
        return new Results();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAppropriateUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cxf.zhq.com/ws/userPairing", name = "getAppropriateUser")
    public JAXBElement<GetAppropriateUser> createGetAppropriateUser(GetAppropriateUser value) {
        return new JAXBElement<GetAppropriateUser>(_GetAppropriateUser_QNAME, GetAppropriateUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAppropriateUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cxf.zhq.com/ws/userPairing", name = "getAppropriateUserResponse")
    public JAXBElement<GetAppropriateUserResponse> createGetAppropriateUserResponse(GetAppropriateUserResponse value) {
        return new JAXBElement<GetAppropriateUserResponse>(_GetAppropriateUserResponse_QNAME, GetAppropriateUserResponse.class, null, value);
    }

}

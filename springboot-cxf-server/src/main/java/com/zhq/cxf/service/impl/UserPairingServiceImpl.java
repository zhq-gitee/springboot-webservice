package com.zhq.cxf.service.impl;

import com.zhq.cxf.annotation.EnableAutoPublish;
import com.zhq.cxf.entity.Results;
import com.zhq.cxf.entity.UserInfo;
import com.zhq.cxf.service.UserPairingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 * @class com.zhq.cxf.service.impl.UserInfoServiceImpl
 * @description 服务实现 @WebService声明此类是一个WebService服务
 * @author zhq
 * @date 2022/6/2 15:40
 * </pre>
 */
@Slf4j
@EnableAutoPublish(publishPath = "/getUser")
@WebService(targetNamespace = "http://cxf.zhq.com/ws/userPairing")
@Service
public class UserPairingServiceImpl implements UserPairingService {


    /**
     * @WebMethod 声明此方法是一个服务方法
     * @WebParam 声明此参数的名称，对应wsdl文档中的参数，不声明会是默认的args0、args1.....
     */
    @WebMethod(action = "http://cxf.zhq.com/ws/UserPairingService/getAppropriateUser")
    @Override
    public Results getAppropriateUser(@WebParam(name = "userInfo") UserInfo userInfo, @WebParam(name = "minAge") int minAge, @WebParam(name = "maxAge") int maxAge) {

        log.info("服务逻辑处理开始。。。");

        // do sth

        System.out.println(userInfo);
        System.out.println(minAge + "<>" + maxAge);

        Results results = new Results();
        List<UserInfo> list = new ArrayList<>();
        int count = 0;
        for (int i = 0; i < 5; i++) {
            UserInfo appropriateUserInfo = new UserInfo();
            appropriateUserInfo.setName("name" + (i + 1));
            list.add(appropriateUserInfo);
            ++ count;
        }
        results.setList(list);
        results.setCount(count);

        log.info("服务逻辑处理结束。。。");

        return results;
    }
}

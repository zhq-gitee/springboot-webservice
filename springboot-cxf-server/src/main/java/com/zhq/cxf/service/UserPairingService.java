package com.zhq.cxf.service;

import com.zhq.cxf.entity.Results;
import com.zhq.cxf.entity.UserInfo;

/**
 * <pre>
 * @class com.zhq.cxf.service.UserInfoService
 * @description 服务接口
 * @author zhq
 * @date 2022/6/2 15:38
 * </pre>
 */
public interface UserPairingService {

    Results getAppropriateUser(UserInfo userInfo, int minAge, int maxAge);

}

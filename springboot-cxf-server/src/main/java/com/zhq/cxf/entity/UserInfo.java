package com.zhq.cxf.entity;

import lombok.Data;

/**
 * <pre>
 * @class com.zhq.cxf.entity.CxfRequest
 * @description
 * @author zhq
 * @date 2022/6/2 15:32
 * </pre>
 */
@Data
public class UserInfo {
    private String name;
    private int age;
    private String sex;
    private double height;
    private double weight;
    private String national;
    private String addr;
}

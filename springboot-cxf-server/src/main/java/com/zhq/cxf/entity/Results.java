package com.zhq.cxf.entity;

import lombok.Data;

import java.util.List;

/**
 * <pre>
 * @class com.zhq.cxf.entity.CxfResponse
 * @description
 * @author zhq
 * @date 2022/6/2 15:32
 * </pre>
 */
@Data
public class Results {

    private List<UserInfo> list;

    private Integer count;

}

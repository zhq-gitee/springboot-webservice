package com.zhq.cxf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <pre>
 * @class com.zhq.cxf.CxfServerApplication
 * @description
 * @author zhq
 * @date 2022/6/2 14:59
 * </pre>
 */
@SpringBootApplication
public class CxfServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CxfServerApplication.class, args);
    }
}

package com.zhq.cxf.config;

import com.zhq.cxf.intercept.AuthHeaderIntercept;
import com.zhq.cxf.service.UserPairingService;
import lombok.extern.slf4j.Slf4j;
import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.ws.Endpoint;

/**
 * <pre>
 * @class com.zhq.cxf.config.CxfConfig
 * @description 手动发布服务配置类
 * @author zhq
 * @date 2022/6/2 20:22
 * </pre>
 */
@Slf4j
// @Configuration
public class CxfConfig {

    @Autowired
    private Bus bus;

    @Autowired
    private UserPairingService userPairingService;

    @Autowired
    private AuthHeaderIntercept auth;


    @Bean
    public Endpoint getAppropriateUser(){
        EndpointImpl endpoint = new EndpointImpl(bus, userPairingService);
        endpoint.publish("/getUser");
        endpoint.getInInterceptors().add(auth);
        log.info("/getUser 服务发布成功！");
        return endpoint;
    }

}

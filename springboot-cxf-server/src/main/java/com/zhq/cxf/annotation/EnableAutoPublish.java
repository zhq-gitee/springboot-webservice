package com.zhq.cxf.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <pre>
 * @class com.zhq.cxf.annotation.EnableAutoPublish
 * @description 自动发布服务注解支持
 * @see com.zhq.cxf.config.AutoPublishConfig
 * @author zhq
 * @date 2022/6/2 15:20
 * </pre>
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface EnableAutoPublish {

    String publishPath();
}
